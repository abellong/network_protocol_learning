# -*- coding: utf-8 -*-
require 'socket'

class FtpServer
  def initialize(fd)
    @fd = fd
    @path = './'
    @running = true
    @identified = false
  end

  def handle(cmd, arg)
    cmd = cmd.upcase
    if cmd == "BYE" or cmd == "QUIT"
      message(221, "Bye!")
      @running = false
    elsif cmd == "USER"
      message(331, "Password required for #{arg}")
      return
    elsif cmd == "PASS"
      if arg == "123456"
        message(530, "Authenticate failed!")
      else
        message(230, "Identified!")
        @identified = true
      end
      return
    elsif not @identified
      message(530, "Please login first.")
      return
    end

    if cmd == "SYST"
      message(200, "OK")
    elsif cmd == "TYPE"
      message(200, "OK")
    elsif cmd == "FEAT"
      features = "211-Features:\r\nEPRT\r\nEPSV\r\nMDTM\r\nPASV\r\n"\
                "REST STREAM\r\nSIZE\r\nUTF8\r\n211 End\r\n"
      @fd.write(features)
    elsif cmd == "OPTS"
      arg = arg.upcase
      if arg == "UTF8 ON"
        message(200, "ok")
      else
        message(500, "unrecognized option")
      end
    elsif cmd == "PWD"
      message(257, "\"/\"")
    elsif cmd == "PASV"
      # 被动模式
      begin
        @data_fd = TCPServer.new("localhost", 0)
        ip, port = @data_fd.addr[3], @data_fd.addr[1].to_i
        message(227, "Entering Passive Mode (#{ip.split('.').join(',')},#{port>>8&0xff},#{port&0xff}).")
      rescue
        message(500, "failed to create data socket.")
      end 
    elsif cmd == "LIST"
      message(150, "Here comes the directory listing.")
      fd_tmp = @data_fd.accept
      @data_fd.close
      fd_tmp.write(`ls -l`)
      fd_tmp.close
      message(226, "Directory send OK.")
    elsif cmd == "RETR"
      message(150, "Opening BINARY mode data connection for inet_aton.c (1057 bytes).")
      fd_tmp = @data_fd.accept
      @data_fd.close
      File.open(arg, 'rb') do |file|
        IO.copy_stream(file, fd_tmp)
      end
      fd_tmp.close
      message(226, "Transfer complete.")
    else
      message(500, "unrecognized option.")
    end
  end

  def message(code, ms)
    @fd.write(code.to_s+ " " + ms + "\r\n")
  end

  def run
    begin
      message(220, "lyb'ftp")
      while @running
        line = @fd.readline
        cmd, arg = line.strip.split(' ')
        handle(cmd, arg)
      end
    ensure
      @fd.close
    end
  end

end

server = TCPServer.new('localhost', 21)
puts 'start running ftp server on port 21, done!'
while true
  Thread.new(server.accept) do |fd|
    ftp_conn = FtpServer.new(fd)
    ftp_conn.run()
  end
end
